from flask import render_template
import pandas as pd
import json


# Create your views here.
def Table(request):
    df = pd.read_csv("C:\\Users\\af71110\Downloads\\python_project\\team\pegaops\\Nextgenblueprods.csv")

    # parsing the DataFrame in json format.
    json_records = df.reset_index().to_json(orient='records')
    data = []
    data = json.loads(json_records)
    context = {'d': data}

    return render_template('table.html', context)