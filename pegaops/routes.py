import pandas as pd
import openpyxl
from tabulate import tabulate
from flask import render_template, redirect, url_for, flash, get_flashed_messages
from pegaops import app
from pegaops.models import Item, User
from pegaops.forms import RegisterForm, Loginform
from pegaops import db
from flask_login import login_user, logout_user


@app.route('/home')
@app.route('/')
def home_page():
    return render_template('/home.html')


@app.route('/team')
def team_page():
    items = Item.query.all()
    return render_template('/team.html', items=items)


@app.route('/nextgenprodgreen')
def nextgenprodgreen_page():
    dfs = pd.read_excel('C:\\Users\\af71110\Downloads\\python_project\\team\\pegaops\\Nextgenprodgreen.xlsx',
                        index_col=0)
    dfs = dfs.fillna('')
    '''return render_template('/data.html', dfs=dfs.to_html())'''
    return render_template('/nextgenprodgreen.html', tables=[dfs.to_html(classes='data')])

@app.route('/nextgennonprod')
def nextgennonprod_page():
    dfs = pd.read_excel('C:\\Users\\af71110\Downloads\\python_project\\team\\pegaops\\Nextgennonprod.xlsx',
                        index_col=0)
    dfs = dfs.fillna('')
    '''return render_template('/data.html', dfs=dfs.to_html())'''
    return render_template('/nextgennonprod.html', tables=[dfs.to_html(classes='data')])


@app.route('/auminonprod')
def auminonprod_page():
    dfs = pd.read_excel('C:\\Users\\af71110\Downloads\\python_project\\team\\pegaops\\auminonprod.xlsx',
                        index_col=0)
    dfs = dfs.fillna('')
    '''return render_template('/data.html', dfs=dfs.to_html())'''
    return render_template('/auminonprod.html', tables=[dfs.to_html(classes='data')])

@app.route('/aumiprod')
def aumiprod_page():
    dfs = pd.read_excel('C:\\Users\\af71110\Downloads\\python_project\\team\\pegaops\\aumi_prod.xlsx',
                        index_col=0)
    dfs = dfs.fillna('')
    '''return render_template('/data.html', dfs=dfs.to_html())'''
    return render_template('/aumiprod.html', tables=[dfs.to_html(classes='data')])


@app.route('/register', methods=['GET', 'POST'])
def register_page():
    form = RegisterForm()
    if form.validate_on_submit():
        user_to_create = User(username=form.username.data,
                              email_address=form.email_address.data,
                              password=form.password1.data)
        db.session.add(user_to_create)
        db.session.commit()
        return redirect(url_for('login_page'))
    if form.errors != {}:  # if there are not errors from validations
        for err_msg in form.errors.values():
            flash(f'There was an error with creating a user: {err_msg}', category='danger')
    return render_template('register.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login_page():
    form = Loginform()
    if form.validate_on_submit():
        attempted_user = User.query.filter_by(username=form.username.data).first()
        if attempted_user and attempted_user.check_password_correction(
                attempted_password=form.password.data
        ):
            login_user(attempted_user)
            flash(f"Success! You have Logged in : {attempted_user.username}", category='Success')
            return redirect(url_for('team_page'))
        else:
            flash('Username and password doesnt match ! Please try again', category='danger')
    return render_template('login.html', form=form)


@app.route('/logout')
def logout_page():
    logout_user()
    flash("You have been logged out", category='Info')
    return redirect(url_for("home_page"))
############################testing###################################
# @app.route('/upload', methods=['GET', 'POST'])
# def upload_page():
#    return render_template('/upload.html')


# @app.route('/data', methods=['GET', 'POST'])
# def data():
#    if request.method == 'POST':
#        file = request.form['uploads']
#        inputs = pd.read_excel(file, index_col=0, engine="openpyxl")
#        return render_template('data.html', data=inputs.to_html())
############################testing###################################
