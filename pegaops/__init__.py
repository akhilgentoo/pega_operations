from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///pega.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'TRUE'
db = SQLAlchemy(app)
app.config['SECRET_KEY'] = '705ce5d75a08735ad0cec437ba83debd'
# if __name__ == '__main__':
#    app.run(debug=True)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
from pegaops import routes
